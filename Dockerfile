FROM php:8.0-fpm

RUN apt-get update && \
  apt-get install -y \
        nginx \
        zlib1g-dev \
        libpng-dev \
        curl \
        libcurl4-openssl-dev \
        pkg-config \
        libssl-dev \
        libzip-dev \
        libpq-dev \
        git \
        zip \
  supervisor

# Install postgres extension docker-php-ext-install pdo mysql
RUN docker-php-ext-install zip \
  && docker-php-ext-install exif \
  && docker-php-ext-install gd \
        && docker-php-ext-install pdo \
        && docker-php-ext-install mysqli \
        && docker-php-ext-install pdo_mysql


# Install composer
RUN curl --silent --show-error https://getcomposer.org/installer | php

#Copy supervisor configuration
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Configure Services and Ports
COPY default /etc/nginx/sites-available/
COPY php.ini /usr/local/etc/php
CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
EXPOSE 80 443
